//
//  Series.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Series: Object, Mappable {
    
    dynamic var available = Int()
    dynamic var collectionURI = ""
    let seriesContext = LinkingObjects(fromType: Character.self, property: "series")
    var seriesItems = List<SeriesItem>()
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Series {
    
    func mapping(map: Map) {
        available <- map["available"]
        collectionURI <- map["collectionURI"]
        seriesItems <- map["items"]
    }
}
