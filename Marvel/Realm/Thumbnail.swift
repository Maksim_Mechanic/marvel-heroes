//
//  Thumbnail.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Thumbnail: Object, Mappable {
    
    dynamic var path = ""
    dynamic var ext = ""
    let imageContext = LinkingObjects(fromType: Character.self, property: "image")
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Thumbnail {
    
    func mapping(map: Map) {
    
        path <- map["path"]
        ext <- map["extension"]
    }
}
