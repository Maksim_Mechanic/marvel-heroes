//
//  URLS.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper


class URLS: Object, Mappable {
    
    dynamic var type = ""
    dynamic var url = ""
    let urls = LinkingObjects(fromType: Character.self, property: "urls")
    
    required convenience init?(map: Map) {
        self.init()
    }

}

extension URLS {
    
    func mapping(map: Map) {
        
        type <- map["type"]
        url <- map["url"]
    }
    
}
