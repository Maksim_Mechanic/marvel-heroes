//
//  Comics.swift
//  Marvel
//
//  Created by Максим on 06.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Comics: Object, Mappable {
  
    dynamic var available = Int()
    dynamic var collectionURI = ""
    let comicsContext = LinkingObjects(fromType: Character.self, property: "comics")
    var comicsItems = List<ComicsItem>()
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Comics {
    
    func mapping(map: Map) {
        
        available <- map["available"]
        collectionURI <- map["collectionURI"]
        comicsItems <- map["items"]
    }

}

