//
//  UrlConstructorForImageRequest.swift
//  Marvel
//
//  Created by Максим on 13.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import Foundation

class UrlConstructorForImageRequest {
    
  static func getFullUrl(imagePath: String, variantImageName: String, imageExtension: String) -> String {
        let fullURLForCharacterImage = imagePath + "/" + variantImageName + "." + imageExtension
        return fullURLForCharacterImage
    }
 
}
