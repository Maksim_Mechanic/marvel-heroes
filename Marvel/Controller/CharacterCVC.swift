//
//  CharacterCVC.swift
//  Marvel
//
//  Created by Максим on 08.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import UIKit
import RealmSwift

class CharacterCVC : UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loadNewDataActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadDataAtStartActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    
    // MARK: - Private
    fileprivate lazy var characterRequestManager = TCharacterRequestManager()
    fileprivate var characters = [Character]()                                    // arrayOfRequestedCharacters
    fileprivate var offset = Int() { didSet { updateUI() } }                      // offsetForRequest
    fileprivate var count = 3                                                     // numberOfResultsReturnedByRequest
    fileprivate var index = Int()  {                                              // indexOfDisplayedCentralCell
        didSet {
            if index >= characters.count - 3 {
                offset += 3
            }
            if index == characters.count - 1 {
                DispatchQueue.main.async(execute: {
                    self.loadNewDataActivityIndicator.startAnimating()
                })
            }
        }
    }
    
    fileprivate func updateUI() {
        characterRequestManager.getCharactersWithResponse(offset: offset, count: self.count) { (charactersData, error) in
            guard error == nil else {
                print("Characters not found! Error : \(error)")
                return
            }
            if let charactersData = charactersData{
                self.characters.append(contentsOf: charactersData)
                var indexPaths = [IndexPath]()
                let from = self.characters.count - charactersData.count
                for i in from...self.characters.count - 1{
                    let indexpath = IndexPath(item: i, section: 0)
                    indexPaths.append(indexpath)
                }
                self.collectionView.insertItems(at: indexPaths)
                DispatchQueue.main.async(execute: {
                    self.loadNewDataActivityIndicator.stopAnimating()
                })
            }
        }
    }
    
    
    fileprivate struct Storyboard {
        static let storyboard = "Main"
        static let characterCellIdentifier = "Character Cell"
        static let detailViewControllerIdentifier = "CharacterDetailViewController"
    }
    
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(CharacterCVC.setBackground(_:)), name: NSNotification.Name(rawValue: "setBackgroundNotification"), object: nil)
        DispatchQueue.main.async(execute: {
            self.loadDataAtStartActivityIndicator.startAnimating()
        })
        characterRequestManager.getCharactersWithResponse(offset: offset, count: count) { (charactersData, error) in
            guard error == nil else {
                print("Characters not found! Error : \(error)")
                return
            }
            if let charactersData = charactersData {
                self.characters.append(contentsOf: charactersData)
                var indexPaths = [IndexPath]()
                for i in 0...self.characters.count - 1{
                    let indexpath = IndexPath(item: i, section: 0)
                    indexPaths.append(indexpath)
                }
                self.collectionView.insertItems(at: indexPaths)
                DispatchQueue.main.async(execute: {
                    self.loadDataAtStartActivityIndicator.stopAnimating()
                    self.loadingLabel.alpha = 0.0
                })
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.itemSize.width = self.view.bounds.width - 80
        flow.itemSize.height = self.view.bounds.height / 1.5
        let inset = CGFloat(self.view.bounds.width - flow.itemSize.width) / 2
        flow.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset)
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setBackground(_ notification: Notification) {
        let image = (notification as NSNotification).userInfo!["image"] as? UIImage
        let url = (notification as NSNotification).userInfo!["imageUrl"] as? String
        DispatchQueue.main.async(execute: {
            if self.index <= self.characters.count - 1{
                let character = self.characters[self.index]
                let characterImagePath = character.getFullImageUrl(variantImageName: VariantImageName.portrait_uncanny)
                if characterImagePath == url {
                    DispatchQueue.main.async(execute: {
                        self.backgroundImage.image = image
                    })
                } else {
                    self.setBackgroundFromFileSystem()
                }
                
            }
        })
        
        
    }
    
    func setBackgroundFromFileSystem() {
        TRequestManager.sharedInstance.concurrentQueue.sync {
            if index <= characters.count - 1{
                let character = characters[index]
                let characterImageUrl = character.getFullImageUrl(variantImageName: VariantImageName.portrait_uncanny)
                let imageView = UIImageView()
                _ = imageView.getImageFromFileSystem(characterImageUrl, complition: { (imageData, imageUrl, _) in
                    if characterImageUrl == imageUrl {
                        if let imageData = imageData{
                            let image = UIImage(data: imageData)
                            DispatchQueue.main.async(execute: {
                                self.backgroundImage.image = image
                            })
                        }
                    }
                })
            }
        }
    }
    
    
}

// MARK: - UICollectionViewDelegate

extension CharacterCVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellv = collectionView.cellForItem(at: indexPath) as! CharacterCVCC
        let image = cellv.characterImageView.image
        let character = self.characters[(indexPath as NSIndexPath).item]
        let sb = UIStoryboard(name: Storyboard.storyboard, bundle: nil)
        let cdvc = sb.instantiateViewController(withIdentifier: Storyboard.detailViewControllerIdentifier) as! CharacterDetailViewController
        cdvc.character = character
        cdvc.image = image
        cdvc.title = character.name
        self.addChildViewController(cdvc)
        cdvc.view.frame = self.view.frame
        self.view.addSubview(cdvc.view)
        cdvc.didMove(toParentViewController: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cellv = cell as! CharacterCVCC
        cellv.resumeDownloadImage()
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cellv = cell as! CharacterCVCC
        cellv.pauseDownloadImage()
        
    }
    
}

// MARK: - UICollectionViewDataSource

extension CharacterCVC : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.characterCellIdentifier, for: indexPath) as! CharacterCVCC
        
        TRequestManager.sharedInstance.concurrentQueue.sync {
            cell.character = self.characters[indexPath.item]
        }
        return cell
    }
    
}

// MARK: - UIScrollViewDelegate

extension CharacterCVC : UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let layout = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let layout = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        let offset = scrollView.contentOffset
        
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        
        let roundedIndex = round(index)
        if ( Int(roundedIndex) > self.index || Int(roundedIndex) < self.index ) && Int(roundedIndex) >= 0 {
            self.index = Int(roundedIndex)
            setBackgroundFromFileSystem()
        }
    }
}
















