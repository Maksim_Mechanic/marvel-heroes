//
//  CharacterDetailViewController.swift
//  Marvel
//
//  Created by Максим on 15.09.16.
//  Copyright © 2016 Максим. All rights reserved.
//

import UIKit

class CharacterDetailViewController : UIViewController {
    
    @IBOutlet weak var characterImageView: UIImageView!
    @IBOutlet weak var characterUid: UILabel!
    @IBOutlet weak var comicsNumber: UILabel!
    @IBOutlet weak var seriesNumber: UILabel!
    @IBOutlet weak var storiesNumber: UILabel!
    @IBOutlet weak var eventsNumber: UILabel!
    @IBOutlet weak var characterBiography: UITextView!
    @IBOutlet weak var characterText: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var image : UIImage?
    var character : Character?
    
    fileprivate struct TransformScaleParameters {
        static let smx = CGFloat(0.6)
        static let smy = CGFloat(0.6)
        
        static let spx = CGFloat(1.3)
        static let spy = CGFloat(1.3)
        
        static let nx = CGFloat(1.0)
        static let ny = CGFloat(1.0)
    }
    
    fileprivate struct AnimationDuration {
        static let duration = TimeInterval(0.5)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showViewWithAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateUI()
    }
    
    @IBAction func closePopUp(_ sender: UIButton) {
        self.removeViewWithAnimation()
    }
    
    fileprivate func updateUI() {
        if let character = self.character {
            DispatchQueue.main.async(execute: {
                if character.desc == "" {
                    self.characterBiography.textColor = UIColor.red
                    self.characterBiography.text = "Information not found"
                } else {
                    self.characterBiography.text = character.desc
                }
                self.characterUid.text = String(character.uid)
                self.comicsNumber.text = String(character.comics!.available)
                self.seriesNumber.text = String(character.series!.available)
                self.storiesNumber.text = String(character.stories!.available)
                self.eventsNumber.text = String(character.events!.available)
                self.characterText.text = character.name
                self.characterImageView.image = self.image
                self.characterImageView.layer.cornerRadius = 60.0
                self.backgroundImageView.image = self.image
            })
        }
    }
    
    fileprivate func showViewWithAnimation() {
        
        self.view.transform = CGAffineTransform(scaleX: TransformScaleParameters.smx, y: TransformScaleParameters.smy)
        self.view.alpha = 0.0
        UIView.animate(withDuration: AnimationDuration.duration, animations:  {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: TransformScaleParameters.nx, y: TransformScaleParameters.ny)
        })
    }
    fileprivate func removeViewWithAnimation() {
        
        UIView.animate(withDuration: AnimationDuration.duration, animations: {
            self.view.transform = CGAffineTransform(scaleX: TransformScaleParameters.smx, y: TransformScaleParameters.smy)
            self.view.alpha = 0.0
            }, completion: { (finished : Bool) in
                if (finished) {
                    self.view.removeFromSuperview()
                }
        })
    }
    
    
}
